import React from "react";

class Post extends React.Component {
  render() {
    return (
      <div className="post">
        <div className="post-title">{this.props.title}</div>
        <div className="post-body">{this.props.body}</div>
        <button onClick={this.editPost}>Edit</button>
        <button onClick={this.deletePost}>Delete</button>
      </div>
    );
  }

  editPost = () => {
    alert("Edit");
  };

  deletePost = () => {
    alert("Delete");
  };
}

export default Post;
