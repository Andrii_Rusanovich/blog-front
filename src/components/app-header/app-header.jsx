import React from "react";
import "./app-header.css";

const AppHeader = () => (
  <div className="header">
    <span className="site-name">Full stack overflow blog</span>
  </div>
);

export default AppHeader;
