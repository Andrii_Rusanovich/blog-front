import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";

ReactDOM.render(
  React.createElement(React.StrictMode, null, [
    React.createElement(App, { key: "StrictMode-App" }),
  ]),
  document.getElementById("root")
);
