import React from "react";
import AppHeader from "./components/app-header/app-header";

function App() {
  return (
    <div>
      <AppHeader />
    </div>
  );
}

export default App;
